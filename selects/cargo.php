<?php   
        require ('../clases/conexion.php');
        function selectGerencia(){
            $obj = new conectar();
            $conexion = $obj->conexion();
            $gerencia = $conexion->real_escape_string ($_POST['gerencia']);
            $query = "SELECT * FROM cargo WHERE cod_gerencia = $gerencia";
            $result = $conexion->query($query);
            $gerencia = '<option value="">Elige una opción</option>';
            while($row = $result->fetch_array(MYSQLI_ASSOC)){
                $gerencia .= '<option value="'."$row[id_cargo]".'">'."$row[des_cargo]".'</option>';
            }
            return $gerencia;
        }
        echo selectGerencia();
?>