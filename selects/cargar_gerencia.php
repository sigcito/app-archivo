<?php   
        require ('../clases/conexion.php');
        function selectGerencia(){
            $obj = new conectar();
            $conexion = $obj->conexion();
            $query = "SELECT * FROM gerencia";
            $result = $conexion->query($query);
            $gerencia = '<option value="">Elige una opción</option>';
            while($row = $result->fetch_array(MYSQLI_ASSOC)){
                $gerencia .= '<option value="'."$row[id_gerencia]".'">'."$row[des_gerencia]".'</option>';
            }
            return $gerencia;
        }
        echo selectGerencia();
?>