<?php
session_start();

if(isset($_SESSION['datos_usuario'])){
    if($_SESSION['datos_usuario']['rol'] != "2"){
        header( "location: admin.php");
        }
    } else {
        header("location: index.php");
    }
    
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Pantalla Usuario</title>
        <?php require_once "estilos.php";
        ?>
        <link rel="stylesheet" href="arreglos.css">
    </head>

    <body class="fondo">
        <!-- Nav begin -->
        <nav class="navbar posicion navbar-dark sticky-top navbarsito">
            <img src="images/logo.png" width="120" height="50" class="inline-block align-top" alt="Logo">
            <div>
                <button class="btn btn-outline-info" type="button" data-toggle="modal" data-target="#modal-registro">Solicitar Registro</button>
                <button class="btn btn-outline-info" type="button" data-target="#modal-registro-prestado" data-toggle="modal"></data-target>Ver estado de Solicitud</button>
                <a class="btn btn-outline-info" href="procesos/cerrar_sesion.php">Cerrar sesion</a>
            </div>
        </nav>
        <!-- end Nav -->

        <!-- TABLA -->

        <div class="container-fluid">
            <div class="table-responsive">
                <div id="tabla_svisualizar"></div>
            </div>
        </div>

        <!-- Modal Solicitar registro -->
        <div class="modal fade" id="modal-registro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">

                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                    </div>
                    <div class="modal-body">
                        <!-- Formulario de solicitar registro -->
                        <h4 class="text-center mb-4">Solicitar Registro</h4>
                        <form id="frmsolicitarregistro">
                            <input class="form-control" type="text" name="id_reg" placeholder="Ingrese el código" id="id_reg">
                            <input class="form-control" type="text" name="status" id="status" value="2">
                            <input class="form-control" type="text" name="id_user" id="id_user" value="<?php echo $_SESSION['datos_usuario']['id_usuario']?>">
                            <div class="row">
                                <div class="col-6">
                                    <label class="">Código de entrada</label>
                                    <input class="form-control" type="text" name="cod_entrada" placeholder="Ingrese el código" id="cod_entrada" disabled>
                                </div>
                                <div class="col-6">
                                    <label>Descripción</label>
                                    <input class="form-control" type="text" name="" placeholder="Ingrese la descripción" id="descripcion" disabled>
                                </div>
                                <div class="col-6">
                                    <label class="">Formato</label>
                                    <input class="form-control" type="text" name="" placeholder="Ingrese el formato" id="formato" disabled>
                                </div>
                                <div class="col-6">
                                    <label class="">Código virgen</label>
                                    <input class="form-control" type="text" name="" placeholder="Ingrese el código virgen" id="cod-virgen" disabled>
                                </div>
                                <div class="col-6">
                                    <label class="">Fecha de grabación</label>
                                    <input class="form-control" type="text" name="" placeholder="Ingrese la fecha de grabación" id="fecha-grabacion" disabled>
                                </div>
                                <div class="col-6">
                                    <label class="">Fecha de salida al aire</label>
                                    <input class="form-control" type="text" name="" placeholder="Ingrese la fecha de salida" id="fecha-salida" disabled>
                                </div>
                                <div class="col-6">
                                    <label class="">Usuario</label>
                                    <input class="form-control" type="text" name="" placeholder="Usuario" id="fecha-grabacion" value="<?php echo $_SESSION['datos_usuario']['nombre'] . " " . $_SESSION['datos_usuario']['apellido']?>" disabled>
                                </div>
                                <div class="col-6">
                                    <label class="">Fecha de solicitud</label>
                                    <input class="form-control" type="date" name="fecha_actual" placeholder="Fecha actual" id="fecha_actual" disabled>
                                </div>
                                <div class="col-6">
                                    <label class="">Fecha devolución material</label>
                                    <input class="form-control" type="date" name="fecha_entrega" placeholder="Fecha devolución material" id="fecha_entrega" disabled>
                                </div>
                                <div class="col-6" mb-4>
                                    <label class=""></label>
                                    <input class="form-control" type="text" name="observacion" placeholder="Fecha devolución material" id="observacion">
                                </div>
                                <hr>
                                <div class="container mt-3">
                                    <div class="float-right">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                        <button type="button" id="solicitar_registro" name="solicitar_registro" class="btn btn-primary">Guardar Cambios</button>
                                    </div>
                                </div>
                                </form>
                                <div class="container-fluid">
                                    <div class="table-responsive">
                                        <div id="tabla-usuario"></div>
                                    </div>
                                </div>

                            </div>
                            <!-- </form> Error -->
                            <!-- end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->

        <!-- ver estado de la solicitud -->
        <div class="modal fade" id="modal-registro-prestado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                    </div>
                    <div class="modal-body">
                        tablas prestadas
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- end modal -->


    </body>
    <?php require_once "scripts.php";?>

    </html>