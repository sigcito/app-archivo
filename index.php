<?php
    session_start();

    if(isset($_SESSION['datos_usuario'])){
        if($_SESSION['datos_usuario']['rol'] == "1"){
            header("Location: admin.php");
        } else if($_SESSION['datos_usuario']['rol'] == "2"){
            header("Location: pantalla-usuario.php");
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Login</title>
        <?php require_once "estilos.php";
        ?>
        <link rel="stylesheet" href="arreglos-login.css">
    </head>
    <body>
        <div class="wallpaper">
            <div class="my-content">
                <div class="container">
    
                    <div class="col-sm-6 col-sm-offset-3 myform-cont posicion">
                        <div class="myform-top">
                            <div class="myform-top-left">
                                <h3>Inicio de Sesión</h3>
    
                            </div>
                            <div class="myform-top-right">
                                <i class="fa fa-key"></i>
                            </div>
                        </div>
                        <div class="myform-bottom">
                            <form role="form" action="" id="formlogin" class="">
                                <div class="form-group">
                                    <input
                                        type="text"
                                        name="usuariolg"
                                        placeholder="Usuario..."
                                        class="form-control"
                                        id="usuariolg"
                                        pattern="[A-Za-z0-9]{1,9}">
                                </div>
                                <div class="form-group">
                                    <input
                                        type="text"
                                        name="passwordlg"
                                        placeholder="Contraseña..."
                                        class="form-control"
                                        id="passwordlg"
                                        pattern="[A-Za-z0-9]{1,9}">
                                </div>
                                <button type="submit" class="mybtn">Entrar</button>
                            </form>
                        </div>
                    </div>
    
                </div>
            </div>
        </div>

        <?php require_once "scripts.php";?>
    </body>
</html>