<?php
require_once "../clases/conexion.php";
$obj = new conectar();
$conexion = $obj -> conexion();

$sql = "SELECT u.id_usuario,     /* 0 */
u.nombre,                        /* 1 */
u.apellido,                      /* 2 */ 
u.cedula,                        /* 3 */ 
u.contrasena,                    /* 4 */
u.carnet,                        /* 5 */
u.cod_gerencia,                  /* 6 */
u.telefono,                      /* 7 */
u.email,                         /* 8 */
u.cod_cargo,                     /* */
c.des_cargo,                     /* */
g.des_gerencia                   /* */
FROM usuario u INNER JOIN gerencia g ON u.cod_gerencia = g.id_gerencia LEFT JOIN cargo c ON u.cod_cargo = c.id_cargo WHERE u.id_usuario ";
 $result=mysqli_query($conexion,$sql);
?>

    <div>
        <table id="tablausuariorico" class="table table-hover custom-table">
            <thead class="thead-dark">
                <tr class="">
                    <th scope="col" class="align-middle">Nombre</th>
                    <th scope="col" class="align-middle tabla arreglo-tabla">Apellido</th>
                    <th scope="col" class="align-middle">cedula</th>
                    <th scope="col" class="align-middle">carnet</th>
                    <th scope="col" class="align-middle">gerencia</th>
                    <th scope="col" class="align-middle">telefono</th>
                    <th scope="col" class="align-middle">E-Mail</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php
            while ($mostrar=mysqli_fetch_row($result)){

                $datos=$mostrar[0]."||".
                        $mostrar[1]."||".
                        $mostrar[2]."||".
                        $mostrar[3]."||".
                        $mostrar[4]."||".
                        $mostrar[5]."||".
                        $mostrar[6]."||".
                        $mostrar[7]."||".
                        $mostrar[8]."||".
                        $mostrar[9];
        ?>
                    <tr>
                        <td>
                            <?php echo $mostrar[1]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[2]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[3]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[4]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[5]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[6]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[7]; ?>
                        </td>
                       
                        
                    </tr>
                    <?php
        }
        ?>
            </tbody>
        </table>
    </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#tablausuariorico').DataTable({
                "language": {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Datos",
                    "infoFiltered": "(Filtrado de _MAX_ total datos)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Datos",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
        });
    </script>