<?php
require_once "../clases/conexion.php";
$obj = new conectar();
$conexion = $obj -> conexion();

$sql = "SELECT id,          
id_registro,                 
fecha_entrada,              
descripcion,                
tipo_medio,                 
cod_virgen,                 
fecha_grabacion,            
fecha_salidaaire,            
cod_usuario,                
status_prestamo             
 from registro WHERE status_prestamo = 3";
 $result=mysqli_query($conexion,$sql);
?>
<hr>
<h4 class="text-center mb-4">Cargar Registro</h4>
    <div>
        <table id="prestadosricos" class="table table-hover custom-table" data-page-length='5'>
            <thead class="thead-dark">
                <tr class="">
                    <th scope="col" class="align-middle">Id Registro</th>                    
                    <th scope="col" class="align-middle">Descripción</th>
                    <th scope="col" class="align-middle">Tipo de Medio</th>
                    <th scope="col" class="align-middle">Código Virgen</th>                   
                    <th scope="col" class="align-middle">Fecha de Salida</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php
            while ($mostrar=mysqli_fetch_row($result)){

                 $datos=$mostrar[0]."||".
                        $mostrar[1]."||".
                        $mostrar[2]."||".
                        $mostrar[3]."||".
                        $mostrar[4]."||".
                        $mostrar[5]."||".
                        $mostrar[6]."||".
                        $mostrar[7]."||".
                        $mostrar[8]."||".
                        $mostrar[9];
        ?>
                    <tr>
                        <td>
                            <?php echo $mostrar[1]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[3]; ?>
                        </td>
                        
                        <td>
                            <?php echo $mostrar[4]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[5]; ?>
                        </td>
                        
                        <td>
                            <?php echo $mostrar[7]; ?>
                        </td>
                        <td style="text-align: center;">
                            <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modificar" onclick="agregaform('<?php echo $datos;?>')">
                            <i class="fas fa-plus-square"></i>
                            </span>
                        </td>
                    </tr>
                    <?php
        }
        ?>
            </tbody>
        </table>
    </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#prestadosricos').DataTable({
                "language": {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Datos",
                    "infoFiltered": "(Filtrado de _MAX_ total datos)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Datos",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });
        });
    </script>