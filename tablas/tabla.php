<?php
require_once "../clases/conexion.php";
$obj = new conectar();
$conexion = $obj -> conexion();

$sql = "SELECT id,          /* 0 */ 
id_registro,                /* 1 */ 
fecha_entrada,              /* 2 */
descripcion,                /* 3 */
tipo_medio,                 /* 4 */
cod_virgen,                 /* 5 */
fecha_grabacion,            /* 6 */
fecha_salidaaire            /* 7 */
 from registro ";
 $result=mysqli_query($conexion,$sql);
?>

    <div>
        <table id="pagina" class="table table-hover custom-table">
            <thead class="thead-dark">
                <tr class="">
                    <th scope="col" class="align-middle">Id Registro</th>
                    <th scope="col" class="align-middle">Fecha de Entrada</th>
                    <th scope="col" class="align-middle tabla arreglo-tabla">Descripción</th>
                    <th scope="col" class="align-middle">Tipo de Medio</th>
                    <th scope="col" class="align-middle">Código Virgen</th>
                    <th scope="col" class="align-middle">Fecha de Grabación</th>
                    <th scope="col" class="align-middle">Fecha de Salida</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php
            while ($mostrar=mysqli_fetch_row($result)){

                $datos=$mostrar[0]."||".
                        $mostrar[1]."||".
                        $mostrar[2]."||".
                        $mostrar[3]."||".
                        $mostrar[4]."||".
                        $mostrar[5]."||".
                        $mostrar[6]."||".
                        $mostrar[7];
        ?>
                    <tr>
                        <td>
                            <?php echo $mostrar[1]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[2]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[3]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[4]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[5]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[6]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[7]; ?>
                        </td>
                        <td style="text-align: center;">
                            <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modificar" onclick="modificar('<?php echo $datos;?>')">
                            <i class="fas fa-plus-square"></i></span>
                        </td>
                        <td style="text-align: center;">
                            <span class="btn btn-danger btn-sm" data-toggle="modal" data-target="#" onclick="eliminarregistro('<?php echo $datos;?>')">
                            <i class="fas fa-trash-alt"></i></span>
                        </td>
                    </tr>
                    <?php
        }
        ?>
            </tbody>
        </table>
    </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#pagina').DataTable({
                "language": {
                    "decimal": "",
                    "emptyTable": "No hay información",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
                    "infoEmpty": "Mostrando 0 to 0 of 0 Datos",
                    "infoFiltered": "(Filtrado de _MAX_ total datos)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Datos",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
        });
    </script>