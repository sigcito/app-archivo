<?php
require_once "../clases/conexion.php";
$obj = new conectar();
$conexion = $obj -> conexion();

$sql = "SELECT s.cod_registro, /* */
r.descripcion, /* */
s.cod_usuario, /* */
u.nombre, /* */
u.apellido, /* */
s.f_prestamo,    /* */            
s.f_reingreso,        /* */         
s.observaciones,           /* */      
s.cod_atendida             /* */
FROM s_visualizar s INNER JOIN registro r ON r.id = s.id_prestamo INNER JOIN usuario u ON u.id_usuario = s.cod_usuario INNER JOIN atendido a ON a.id_atendido = s.cod_atendida WHERE s.id_prestamo";
 $result=mysqli_query($conexion,$sql);
?>
<h4 class="text-center mt-4  mb-4">Solicitudes</h4>
    <div>
        <table id="prestadosricos" class="table table-hover custom-table">
            <thead class="thead-dark">
                <tr class="">
                    <th scope="col" class="align-middle">Usuario</th>                    
                    <th scope="col" class="align-middle">Material solicitado</th>
                    <th scope="col" class="align-middle">Fecha de solicitud</th>
                    <th scope="col" class="align-middle">Fecha de reingreso</th>                   
                    <th scope="col" class="align-middle">Estado de solicitud</th>
                    <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                <?php
            while ($mostrar=mysqli_fetch_row($result)){

                 $datos=$mostrar[0]."||".
                        $mostrar[1]."||".
                        $mostrar[2]."||".
                        $mostrar[3]."||".
                        $mostrar[4]."||".
                        $mostrar[5]."||".
                        $mostrar[6]."||".
                        $mostrar[7]."||".
                        $mostrar[8];
        ?>
                    <tr>
                        <td>
                            <?php echo $mostrar[1]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[3]; ?>
                        </td>
                        
                        <td>
                            <?php echo $mostrar[4]; ?>
                        </td>
                        <td>
                            <?php echo $mostrar[5]; ?>
                        </td>
                        
                        <td>
                            <?php echo $mostrar[7]; ?>
                        </td>
                        <td style="text-align: center;">
                            <span class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modificar" onclick="agregaform('<?php echo $datos;?>')">
                            <i class="fas fa-plus-square"></i>
                            </span>
                        </td>
                    </tr>
                    <?php
        }
        ?>
            </tbody>
        </table>
    </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#prestadosricos').DataTable({
                "language": {
                    "decimal": "",
                    "emptyTable": "No ha hecho ninguna solicitud",
                    "info": "Mostrando _START_ a _END_ de _TOTAL_ Datos",
                    "infoEmpty": "Mostrando 0 a 0 de 0 Datos",
                    "infoFiltered": "(Filtrado de _MAX_ total datos)",
                    "infoPostFix": "",
                    "thousands": ",",
                    "lengthMenu": "Mostrar _MENU_ Datos",
                    "loadingRecords": "Cargando...",
                    "processing": "Procesando...",
                    "search": "Buscar:",
                    "zeroRecords": "Sin resultados encontrados",
                    "paginate": {
                        "first": "Primero",
                        "last": "Ultimo",
                        "next": "Siguiente",
                        "previous": "Anterior"
                    }
                }
            });
            $('#datepicker').datepicker({
                uiLibrary: 'bootstrap4'
            });
        });
    </script>