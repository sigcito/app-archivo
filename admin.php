<?php
session_start();

if(isset($_SESSION['datos_usuario'])){
    if($_SESSION['datos_usuario']['rol'] != "1"){
        header("location: pantalla-usuario.php");
        }
    } else {
        header("location: index.php");
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Sistema de Archivo</title>
    <?php require_once "estilos.php";
        ?>
    <link rel="stylesheet" href="arreglos.css">
</head>

<body class="fondo">
    <!-- Nav begin -->
    <nav class="navbar posicion navbar-dark sticky-top navbarsito">
        <img src="images/logo.png" width="120" height="50" class="inline-block align-top" alt="Logo">
        <div>
            <button class="btn btn-outline-info" type="button" data-toggle="modal" data-target="#modal-registro">Ingresar Registro</button>
            <button class="btn btn-outline-info" type="button" data-target="#modal-registro-prestado" data-toggle="modal"></data-target>Consultar Registros Prestados</button>
            <button class="btn btn-outline-info" type="button" data-target="#modal-solicitudes" data-toggle="modal"></data-target>Solicitudes Pendientes</button>
            <button class="btn btn-outline-info" type="button" data-target="#modal-registro-usuario" data-toggle="modal"></data-target>Registro de usuario</button>
            <button class="btn btn-outline-info" type="button" data-target="#modal-ver-usuarios" data-toggle="modal"></data-target>Ver usuarios</button>
            <a class="btn btn-outline-info"  href="procesos/cerrar_sesion.php">Cerrar sesion</a>

        </div>
    </nav>
    <!-- end Nav -->
    <div class="container-fluid">
        <div class="table-responsive">
            <div id="tablaDatatable"></div> 
        </div>
    </div>
    <!-- Modal registro -->
    <div class="modal fade" id="modal-registro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                </div>
                <div class="modal-body">
                    <!-- Formulario de ingresar registro -->
                    <h4 class="text-center mb-4">Ingresar registro</h4>
                    <form id="ingresar_registro" class="needs-validation" novalidate>
                        <div class="row">
                            <div class="col-6">
                                <label for="validacion_entrada" class="">Código de entrada</label>
                                <input class="form-control" type="text" name="validacion_entrada" placeholder="Ingrese el código" id="validacion_entrada"
                                    required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_descripcion">Descripción</label>
                                <input class="form-control" type="text" name="validacion_descripcion" placeholder="Ingrese la descripción" id="validacion_descripcion"
                                    required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="" for="validacion_fechaentrada">Fecha de entrada</label>
                                <input class="form-control" onfocus="this.type='date'" type="text" name="validacion_fechaentrada" placeholder="Ingrese la fecha de entrada"
                                    id="validacion_fechaentrada" required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_formato" class="">Formato</label>
                                <input class="form-control" type="text" name="validacion_formato" placeholder="Ingrese el formato" id="validacion_formato"
                                    list="lista1" required>
                                <datalist id="lista1">
                                    <option value="DVCPRO">
                                        <option value="BETACAM">
                                            <option value="CD">
                                </datalist>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>

                            </div>
                            <div class="col-6">
                                <label for="validacion_virgen" class="">Código virgen</label>
                                <input class="form-control" type="text" name="validacion_virgen" placeholder="Ingrese el código virgen" id="validacion_virgen"
                                    required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_grabacion" class="">Fecha de grabación</label>
                                <input class="form-control" onfocus="this.type='date'" type="text" name="validacion_grabacion" placeholder="Ingrese la fecha de grabación"
                                    id="validacion_grabacion" required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_salida" class="">Fecha de salida al aire</label>
                                <input class="form-control" onfocus="this.type='date'" type="text" name="validacion_salida" placeholder="Ingrese la fecha de salida"
                                    id="validacion_salida" required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback"> 
                                    Luce bien
                                </div>
                            </div>                            

                        </div>
                        <hr>
                        <div class="float-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-primary" id="agregar-registro">Agregar nuevo</button>
                        </div>
                    </form>
                    <!-- end -->
                </div>

            </div>
        </div>
    </div>
    <!-- end modal -->

    <!-- Modal registros prestados -->
    <div class="modal fade" id="modal-registro-prestado" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="table-responsive">
                            <div id="tabla_prestada"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->

    <!-- Modal solicitudes -->

    <div class="modal fade" id="modal-solicitudes" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <div class="container-fluid">
                            <div id="tabla_solicitudes"></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->

    <!-- Modal registro de usuario -->

    <div class="modal fade" id="modal-registro-usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Registrar nuevo usuario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                </div>
                <div class="modal-body">
                    <!-- Formulario de ingresar registro -->
                    <form class="needs-validation" id="registrar_usuario" novalidate>
                        <div class="row">
                            <div class="col-6">
                                <label for="validacion_nombre" class="">Nombre</label>
                                <input class="form-control" type="text" name="validacion_nombre" placeholder="Ingrese su nombre" id="validacion_nombre" required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_apellido">Apellido</label>
                                <input class="form-control" type="text" name="validacion_apellido" placeholder="Ingrese su apellido" id="validacion_apellido" required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_cedula" class="">Cedula</label>
                                <input class="form-control" type="text" name="validacion_cedula" placeholder="Ingrese su cédula" id="validacion_cedula" required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_carnet" class="">Código de carnet</label>
                                <input class="form-control" type="text" name="validacion_carnet" placeholder="Ingrese su código de carnet" id="validacion_carnet" required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_telefono">Teléfono</label>
                                <input class="form-control" type="text" name="validacion_telefono" placeholder="Ingrese su teléfono" id="validacion_telefono" required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>

                            </div>
                            <div class="col-6">
                                <label for="validacion_correo">Correo</label>
                                <input class="form-control" type="text" name="validacion_correo" placeholder="Ingrese su correo" id="validacion_correo" required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col">
                                <label>Gerencia</label>
                                <select class="form-control" id="validacion_gerencia" name="validacion_gerencia" required>
                                 <option value="">Seleccione su Gerencia</option>                                 
                                 </select>
                                 <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col">
                                <label for="validacion_cargo">Cargo</label>
                                <select class="form-control" id="validacion_cargo" name="validacion_cargo" required>
                                <option value="">Seleccione su Cargo</option>
                                </select>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_contraseña" class="">Contraseña</label>
                                <input class="form-control" type="text" name="validacion_contraseña" placeholder="Ingrese la contraseña"
                                    id="validacion_contraseña" required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_rol">Rol</label>
                                <select class="form-control" id="validacion_rol" name="validacion_rol" required>
                                <option value="">Seleccione su Cargo</option>
                                </select>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="float-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary" id="guardar_usuario">Registrar Usuario</button>
                        </div>
                    </form>
                    <!-- end -->
                </div>
            </div>
        </div>
    </div>
    <!-- end modal -->

    <!-- Modal ver usuario -->

    <div class="modal fade" id="modal-ver-usuarios" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Usuarios Registrados</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <div class="container-fluid">
                            <div id="tabla_usuarios"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- end modal -->
     <!-- Modal modificar -->
     <div class="modal fade" id="modificar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
                </div>
                <div class="modal-body">
                    <!-- Formulario modificar -->
                    <h4 class="text-center mb-4">Modificar Registro</h4>
                    <form id="ingresar_registroM" class="needs-validation" novalidate>
                    <input type="hidden" id="id_m" name="id_m">
                        <div class="row">
                            <div class="col-6">
                                <label for="validacion_entrada" class="">ID Registro</label>
                                <input class="form-control" type="text" name="validacion_entradaM" placeholder="Ingrese el código" id="validacion_entradaM"
                                    required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_descripcion">Fecha de Entrada</label>
                                <input class="form-control" type="text" name="validacion_fechaentradaM" placeholder="Ingrese la descripción" id="validacion_descripcionM"
                                    required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="" for="validacion_fechaentrada">Descripción</label>
                                <input class="form-control"  type="text" name="validacion_descripcionM" placeholder="Ingrese la fecha de entrada"
                                    id="validacion_fechaentradaM" required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_formato" class="">Tipo de Medio</label>
                                <input class="form-control" type="text" name="validacion_formatoM" placeholder="Ingrese el formato" id="validacion_formatoM"
                                    list="lista1" required>
                                <datalist id="lista1">
                                    <option value="DVCPRO">
                                        <option value="BETACAM">
                                            <option value="CD">
                                </datalist>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_virgen" class="">Código Virgen</label>
                                <input class="form-control" type="text" name="validacion_virgenM" placeholder="Ingrese el código virgen" id="validacion_virgenM"
                                    required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_grabacion" class="">Fecha de Grabación</label>
                                <input class="form-control" onfocus="this.type='date'" type="text" name="validacion_grabacionM" placeholder="Ingrese la fecha de grabación"
                                    id="validacion_grabacionM" required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>
                            <div class="col-6">
                                <label for="validacion_salida" class="">Fecha de Salida</label>
                                <input class="form-control" onfocus="this.type='date'" type="text" name="validacion_salidaM" placeholder="Ingrese la fecha de salida"
                                    id="validacion_salidaM" required>
                                <div class="invalid-feedback">
                                    Debe llenar los campos
                                </div>
                                <div class="valid-feedback">
                                    Luce bien
                                </div>
                            </div>                            
                        </div>
                        <hr>
                        <div class="float-right">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            <button type="button" class="btn btn-primary" id="agregar-registroM" data-dismiss="modal" >Modificar</button>
                        </div>
                    </form>
                    <!-- end -->
                </div>

            </div>
        </div>
    </div>
    <!-- end modal -->
    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function () {
            'use strict';
            window.addEventListener('load', function () {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function (form) {
                    form.addEventListener('submit', function (event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
</body>
<?php require_once "scripts.php";?>
</html>