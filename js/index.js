// LOGIN

jQuery(document).on('submit','#formlogin', function(event){
    event.preventDefault();

    jQuery.ajax({
        url: 'loginproceso.php',
        type: 'POST',
        dataType: 'json',
        data: $(this).serialize(),
        beforeSend: function(){    
            
        }
    })
    .done(function(datos){
        console.log(datos);
        if(!datos.error){
            if(datos.rol == '1'){
                location.href = 'admin.php';
            }else if(datos.rol == '2'){
                location.href = 'pantalla-usuario.php';                
            }
        }else {
            alertify.error('Los datos que ha ingresado no son correctos o no existen, verifíque sus datos.');
        }
    })
    .fail(function(resp){
        console.log(resp.responseText);
    })
    .always(function(){
        console.log('Complete');
    });
});

// RELLENAR FORM (PANTALLA USUARIO > SOLICITAR REGISTRO)

function agregaform (datos){
    d=datos.split('||');
    $('#id_reg').val(d[0]);        
    $('#cod_entrada').val(d[1]);
    $('#descripcion').val(d[3]);
    $('#formato').val(d[4]);
    $('#cod-virgen').val(d[5]);
    $('#fecha-grabacion').val(d[6]);
    $('#fecha-salida').val(d[7]);
}
// tabla modificar 

function modificar (datos){
    d=datos.split('||');
    $('#id_m').val(d[0]);
    $('#validacion_entradaM').val(d[1]);        
    $('#validacion_descripcionM').val(d[2]);
    $('#validacion_fechaentradaM').val(d[3]);
    $('#validacion_formatoM').val(d[4]);
    $('#validacion_virgenM').val(d[5]);
    $('#validacion_grabacionM').val(d[6]);
    $('#validacion_salidaM').val(d[7]);
}
        //Boton de actualizar 
        $('#agregar-registroM').click(function(){
            datos=$('#ingresar_registroM').serialize();

            $.ajax({
                type:"POST",
                data:datos,
                url:"procesos/actualizar.php",
                success:function(r){
                    if(r==1){
                        $('#tablaDatatable').load('tablas/tabla.php');
                        alertify.success("Actualizado con exito.");
                    }else{
                        alertify.error("Fallo al actualizar registro.")
                    }
                }
            })
        });

// AGREGAR REGISTRO

$(document).ready(function(){
    $('#agregar-registro').click(function(){
        var validacion_entrada = $("#validacion_entrada").val();
        var validacion_descripcion = $("#validacion_descripcion").val();
        var validacion_fechaentrada = $("#validacion_fechaentrada").val();
        var validacion_formato = $("#validacion_formato").val();
        var validacion_virgen = $("#validacion_virgen").val();
        var validacion_grabacion = $("#validacion_grabacion").val();
        var validacion_salida = $("#validacion_salida").val();
        
/* id del form */

        var datos=$('#ingresar_registro').serialize();
        //console.log(datos);
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/ingresar-registro.php",
            success: function(r){
                if(r==1){
                    $('#tablaDatatable').load('tablas/tabla.php');
                    alertify.success("Agregado con exito");
                }
                else{
                    alertify.error("Fallo al agregar datos.");
                    //console.log(validacion_entrada);
                    //console.log(datos);
                }
            }
        });
    });
});

/* REGISTRAR USUARIO */

$(document).ready(function(){
    $('#guardar_usuario').click(function(){
        var validacion_nombre = $("#validacion_nombre").val();
        var validacion_apellido = $("#validacion_apellido").val();
        var validacion_cedula = $("#validacion_cedula").val();
        var validacion_carnet = $("#validacion_carnet").val();        
        var validacion_telefono = $("#validacion_telefono").val();
        var validacion_correo = $("#validacion_correo").val();
        var validacion_gerencia = $("#validacion_gerencia").val();
        var validacion_gerencia = $("#validacion_cargo").val();        
        var validacion_contraseña = $("#validacion_contraseña").val();
        var validacion_rol = $("#validacion_rol").val();

/* id del form */

        var datos=$('#registrar_usuario').serialize();
        //console.log(datos);
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/registrar-usuario.php",
            success: function(r){
                if(r==1){
                    $('#tablaDatatable').load('tablas/tabla.php');
                    alertify.success("Usuario agregado con exito.");
                }
                else{
                    alertify.error("Fallo al agregar datos.");
                    //console.log(validacion_entrada);
                    //console.log(datos);
                }
            }
        });
    });
});

/* SOLICITAR REGISTRO */

$(document).ready(function(){
    $('#solicitar_registro').click(function(){
        var id_reg = $("#id_reg").val();
        var status = $("#status").val();
        var id_user = $("#id_user").val();
        var fecha_actual = $("#fecha_actual").val();        
        var fecha_entrega = $("#fecha_entrega").val();
        var observacion = $("#observacion").val();

/* id del form */

        var datos=$('#frmsolicitarregistro').serialize();
        //console.log(datos);
        $.ajax({
            type: "POST",
            data: datos,
            url: "procesos/solicitar_registro.php",
            success: function(r){
                if(r==1){
                    $('#tablaDatatable').load('tablas/tabla.php');
                    alertify.success("Usuario agregado con exito.");
                }
                else{
                    alertify.error("Fallo al agregar datos.");
                    //console.log(validacion_entrada);
                    //console.log(datos);
                }
            }
        });
    });
});

/* ELIMINAR DATOS */

function eliminarregistro(id) {
    alertify.confirm('Eliminar datos', '¿Esta realmente seguro de eliminar este registro?', function () {
        eliminardatos(id)
    }, function () {
        // alertify.error('Se ha cancelado la operacion')
    });
}

function eliminardatos(id) {

    cadena = "id=" + id;
    $.ajax({
        type: "POST",
        url: "procesos/eliminar_registro.php",
        data: cadena,
        success: function (r) {
            if (r == 1) {
                $('#tablaDatatable').load('tablas/tabla.php');
                alertify.success("Registro eliminado con exito.");
            } else {
                alertify.error("Error al eliminar registro.");
            }
        }
    });
}

    /* FECHA ACTUAL */

$(document).ready(function() {
    var date = new Date();

    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    if (month < 10) month = "0" + month;
    if (day < 10) day = "0" + day;

    var today = year + "-" + month + "-" + day;       
    $("#fecha_actual").attr("value", today);
});

    /* FECHA DE ENTREGA */

$(document).ready(function() {
    var date = new Date();

    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();

    if (month < 10) month = "0" + month;
    if (day < 10) day = "1" + day;

    var today = year + "-" + month + "-" + day;       
    $("#fecha_entrega").attr("value", today);
});

// javascript:R=0; x1=.1; y1=.05; x2=.25; y2=.24; x3=1.6; y3=.24; x4=300; y4=200; x5=300; y5=200; DI=document.getElementsByTagName("img"); DIL=DI.length; function A(){for(i=0; i-DIL; i++){DIS=DI[ i ].style; DIS.position='absolute'; DIS.left=(Math.sin(R*x1+i*x2+x3)*x4+x5)+"px"; DIS.top=(Math.cos(R*y1+i*y2+y3)*y4+y5)+"px"}R++}setInterval('A()',5); void(0);