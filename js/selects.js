$(document).ready(function () {

    /* SELECT CARGAR GERENCIA*/

    $.ajax({
            type: 'POST',
            url: 'selects/cargar_gerencia.php',
        })
        .done(function (gerencia) {
            $('#validacion_gerencia').html(gerencia)
        })
        .fail(function () {
            alert('Error al cargar gerencia')
        })

    /* SELECT DEPENDIENTE */ 

    $('#validacion_gerencia').on('change', function(){
        var gerencia = $('#validacion_gerencia').val();
          $.ajax({
                type: 'POST',
                url: 'selects/cargo.php',
                data: {'gerencia': gerencia}
            })
            .done(function(gerencia){
                $('#validacion_cargo').html(gerencia)
            })
            .fail(function(){
                alert('Error al cargar toners.')
            })  
    })

    /* SELECT ROL */

    $.ajax({
        type: 'POST',
        url: 'selects/cargar_rol.php',
    })
    .done(function (rol) {
        $('#validacion_rol').html(rol)
    })
    .fail(function () {
        alert('Error al cargar rol')
    })
})